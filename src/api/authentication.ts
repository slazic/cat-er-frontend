import axios from "axios";
import { SignInUser } from "../forms/SignInForm";
import { SignUpUser } from "../forms/SignUpForm";

const BASE_URL = "http://localhost:8080/api/v1";

const signIn = async (values: SignInUser) => {
  return await axios.post(BASE_URL + "/authentication/login", values);
};

const signUp = async (values: SignUpUser) => {
  return await axios.post(BASE_URL + "/authentication/register", values);
};

const signOut = async () => {
  return await axios.post(BASE_URL + "/logout");
};

export { signIn, signUp, signOut };
