import axios from "axios";
import { Jwt } from "../context/AuthUtility";

const BASE_URL = "http://localhost:8080/api/v1";

//const token = sessionStorage.getItem("token");

const fetchAllUsers = async (token: Jwt) => {
  return await axios.get(BASE_URL + "/user/getAll", {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const changeUserAccess = async (userId: number, newAccessStatus: boolean, token: Jwt) => {
  return await axios.put(BASE_URL + `/user/changeUserAccess`, null, {
    params: { id: userId, access: newAccessStatus },
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export { fetchAllUsers, changeUserAccess };
