type SignUpResponse = {
  data: string;
  status: number;
  statusText: string;
};

type SignInResposne = {
  data: string;
  status: number;
  statusText: string;
};

type SingleErrorResponse = {
  data: string;
  status: number;
  statusText: string;
};

export { type SignInResposne, type SignUpResponse, type SingleErrorResponse };
