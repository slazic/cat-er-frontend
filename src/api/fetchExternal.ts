import axios from "axios";
import { Jwt } from "../context/AuthUtility";

const BASE_URL = "http://localhost:8080/api/v1";

//const token = sessionStorage.getItem("token");

const fetch5MinIntervalCandlestickDataFromBinance = async (token: Jwt) => {
  return await axios.get(BASE_URL + "/fetchExternal/binance5MinInterval?symbols=BTCEUR,ETHEUR,XRPEUR,ADAEUR,LTCEUR", {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const fetchSingleCandlestickDataFromSelectedMarket = async (
  symbol: string | undefined,
  timeframe: string | undefined,
  exchangeName: string | undefined,
  token: Jwt
) => {
  return await axios.get(BASE_URL + "/fetchExternal/singleCandlestickDataFromSelectedMarket", {
    params: { symbol: symbol, timeframe: timeframe, exchangeName: exchangeName },
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const fetchTickerDataFromAllExchanges = async (symbol: string | undefined, token: Jwt) => {
  return await axios.get(BASE_URL + "/fetchExternal/tickerDataFromAllExchanges", {
    params: { symbol: symbol },
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export {
  fetch5MinIntervalCandlestickDataFromBinance,
  fetchSingleCandlestickDataFromSelectedMarket,
  fetchTickerDataFromAllExchanges,
};
