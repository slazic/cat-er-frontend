interface TooltipWrapperProps {
  children: React.ReactNode;
  title: string;
}

const TooltipWrapper: React.FC<TooltipWrapperProps> = ({
  children,
  title,
}): JSX.Element => {
  return (
    <div className="relative group inline-block">
      {children}
      <div className="min-w-full whitespace-nowrap absolute left-1/2 transform -translate-x-1/2 mt-2 px-2 py-1 text-xs text-white bg-gray-800 rounded shadow-lg opacity-0 group-hover:opacity-100 transition duration-200 ease-in-out text-center">
        {title}
        <div className="absolute left-1/2 transform -translate-x-1/2 bottom-full w-0 h-0 border-l-[7px] border-l-transparent border-b-[5px] border-b-gray-800 border-r-[7px] border-r-transparent"></div>
      </div>
    </div>
  );
};

export default TooltipWrapper;
