import { useContext, useState } from "react";
import * as Yup from "yup";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";

import { Formik, Form, Field, ErrorMessage, FormikHelpers } from "formik";
import { Link, useLocation } from "react-router-dom";

import { useNavigate } from "react-router-dom";
import { SignInResposne, SingleErrorResponse } from "../api/types";
import { AuthContext } from "../context/AuthProvider";
import { signIn } from "../api/authentication";
import { toast } from "react-toastify";

export interface SignInFormProps {
  email: string;
  password: string;
}

export type SignInUser = {
  email: string;
  password: string;
};

const SignInSchema = Yup.object().shape({
  email: Yup.string().required("This field is required."),
  password: Yup.string().required("This field is required."),
});

const SignInForm = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const from = location.state?.from?.pathname || "/";
  const { saveToken } = useContext(AuthContext);
  const [showPassword, setShowPassword] = useState(false);
  const [isFocused, setIsFocused] = useState(false);
  let responseBody: SignInResposne | undefined;
  let responseError: SingleErrorResponse | undefined;

  const onShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const toggleFocus = (value: boolean) => {
    setIsFocused(value);
  };

  const onSubmit = async (values: SignInFormProps, formikHelpers: FormikHelpers<SignInFormProps>) => {
    const user: SignInUser = {
      email: values.email,
      password: values.password,
    };

    await signIn(user)
      .then((response) => {
        responseBody = response;
        toast.success("Successful login.", {
          position: "top-center",
          autoClose: 3000,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
        });
      })
      .catch((error) => {
        responseError = error.response;
      });

    if (responseError && responseError.status === 403) {
      console.log(responseError.data);
    } else if (responseError && responseError.status === 400) {
      formikHelpers.setFieldError("password", responseError.data);
    } else if (responseError && responseError.status === 423) {
      toast.error(responseError.data, {
        position: "top-center",
        autoClose: 3000,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
      });
    } else if (responseBody && responseBody.status === 200) {
      saveToken(responseBody.data);
      navigate(from, { replace: true });
    }
  };

  return (
    <Formik
      initialValues={{
        email: "",
        password: "",
      }}
      validationSchema={SignInSchema}
      onSubmit={(values, formikHelpers) => onSubmit(values, formikHelpers)}
    >
      <Form>
        <div className="grid grid-cols-1 justify-items-center gap-4">
          <Field name="email" type="email" placeholder="E-mail" className="rounded-md py-4 ps-4 bg-neutral-100 w-2/3" />
          <ErrorMessage name="email" component="div" className="text-red-500" />

          <div
            className={
              isFocused
                ? "flex items-center bg-neutral-100 w-2/3 rounded-md outline outline-2 outline-blue-600"
                : "flex items-center bg-neutral-100 w-2/3 rounded-md"
            }
            onFocus={() => toggleFocus(true)}
            onBlur={() => toggleFocus(false)}
          >
            <Field
              name="password"
              type={showPassword ? "text" : "password"}
              placeholder="Password"
              className="rounded-md py-4 ps-4 bg-neutral-100 w-full focus:outline-none"
            />
            <button type="button" onClick={() => onShowPassword()} className="px-5">
              {showPassword ? (
                <FontAwesomeIcon icon={faEye} size="lg" />
              ) : (
                <FontAwesomeIcon icon={faEyeSlash} size="lg" />
              )}
            </button>
          </div>

          <ErrorMessage name="password" component="div" className="text-red-500" />

          <div className="text-center mt-4">
            <Link to="/" className="underline text-blue-500">
              Forgot your password?
            </Link>
          </div>

          <button
            type="submit"
            className="rounded-md bg-amber-600 text-white text-base font-bold py-3 my-8 w-2/3 hover:xl:shadow-lg xl:w-3/12"
          >
            Sign in
          </button>
        </div>
      </Form>
    </Formik>
  );
};

export default SignInForm;
