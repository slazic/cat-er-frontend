import React, { useState } from "react";

import * as Yup from "yup";
import { ErrorMessage, Field, Form, Formik, FormikHelpers } from "formik";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";

import { useNavigate } from "react-router-dom";
import { SignUpResponse, SingleErrorResponse } from "../api/types";
import { signUp } from "../api/authentication";

export interface SignUpFormProps {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  termsOfUse: boolean;
}

export type SignUpUser = {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
};

const SignUpSchema = Yup.object().shape({
  firstName: Yup.string()
    .matches(/^[a-žA-Ž]*$/u, "First name can include only letters.")
    .max(128, "First name can contain max 128 letters.")
    .required("This field is required."),

  lastName: Yup.string()
    .matches(/^[a-žA-Ž]*$/u, "Last name can include only letters.")
    .max(128, "Last name can contain max 128 letters.")
    .required("This field is required."),

  email: Yup.string()
    .email("E-mail address is not valid.")
    .max(60, "E-mail address can contain max 60 characters.")
    .required("This field is required."),

  password: Yup.string()
    .min(8, "Password must contain at least 8 characters")
    .required("This field is required."),

  termsOfUse: Yup.bool().oneOf(
    [true],
    "You must accept the terms and conditions."
  ),
});

const SignUpForm = () => {
  const navigate = useNavigate();
  const [showPassword, setShowPassword] = useState(false);
  const [isFocused, setIsFocused] = useState(false);

  let responseBody: SignUpResponse | undefined;
  let responseError: SingleErrorResponse | undefined;

  const onShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const toggleFocus = (value: boolean) => {
    setIsFocused(value);
  };

  const onSubmit = async (
    values: SignUpFormProps,
    formikHelpers: FormikHelpers<SignUpFormProps>
  ) => {
    const user: SignUpUser = {
      firstName: values.firstName,
      lastName: values.lastName,
      email: values.email,
      password: values.password,
    };

    await signUp(user)
      .then((response) => {
        responseBody = response;
      })
      .catch((error) => {
        responseError = error.response;
      });
    if (responseError && responseError.status === 409) {
      formikHelpers.setFieldError("email", responseError.data);
    } else if (responseError && responseError.status === 400) {
      formikHelpers.setFieldError("email", responseError.data);
    } else if (responseBody && responseBody.status === 201) {
      navigate("/thank_you");
    }
  };

  return (
    <Formik
      initialValues={{
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        termsOfUse: true,
      }}
      validationSchema={SignUpSchema}
      onSubmit={(values, formikHelpers) => onSubmit(values, formikHelpers)}
    >
      <Form>
        <div className="grid grid-cols-1 justify-items-center gap-4">
          <div className="w-2/3 grid grid-cols-1 gap-4 xl:grid xl:grid-cols-2 xl:gap-4">
            <div className="">
              <Field
                name="firstName"
                placeholder="First name"
                className="rounded-md py-4 ps-4 bg-neutral-100 w-full"
              />
              <ErrorMessage
                name="firstName"
                component="div"
                className="text-center text-red-500 pt-4"
              />
            </div>

            <div className="">
              <Field
                name="lastName"
                placeholder="Last name"
                className="rounded-md py-4 ps-4 bg-neutral-100 w-full"
              />
              <ErrorMessage
                name="lastName"
                component="div"
                className="text-center text-red-500 pt-4"
              />
            </div>
          </div>
          <Field
            name="email"
            type="email"
            placeholder="E-mail"
            className="rounded-md py-4 ps-4 bg-neutral-100 w-2/3"
          />
          <ErrorMessage name="email" component="div" className="text-red-500" />
          <div
            className={
              isFocused
                ? "flex items-center bg-neutral-100 w-2/3 rounded-md outline outline-2 outline-blue-600"
                : "flex items-center bg-neutral-100 w-2/3 rounded-md"
            }
            onFocus={() => toggleFocus(true)}
            onBlur={() => toggleFocus(false)}
          >
            <Field
              name="password"
              type={showPassword ? "text" : "password"}
              placeholder="Password"
              className="rounded-md py-4 ps-4 bg-neutral-100 w-full focus:outline-none"
            />
            <button
              type="button"
              onClick={() => onShowPassword()}
              className="px-5"
            >
              {showPassword ? (
                <FontAwesomeIcon icon={faEye} size="lg" />
              ) : (
                <FontAwesomeIcon icon={faEyeSlash} size="lg" />
              )}
            </button>
          </div>
          <ErrorMessage
            name="password"
            component="div"
            className="text-red-500"
          />
          <label className="px-4 text-justify">
            <Field name="termsOfUse" type="checkbox" />
            <span className="ps-4">
              By continuing you accept our
              <span className="underline text-blue-500"> Privacy Policy </span>
              and
              <span className="underline text-blue-500"> Terms of Use.</span>
            </span>
          </label>
          <ErrorMessage
            name="termsOfUse"
            component="div"
            className="text-red-500"
          />
          <button
            type="submit"
            className="rounded-md bg-amber-600 text-white text-base font-bold py-3 my-8 w-2/3 hover:xl:shadow-lg xl:w-3/12"
          >
            Sign up
          </button>
        </div>
      </Form>
    </Formik>
  );
};

export default SignUpForm;
