import { useRef } from "react";

export const useDetails = () => {
  const chartRef = useRef<any>(null);

  const scrollToZoom = (event: any) => {
    event.preventDefault(); // Sprječava scroll stranice

    if (chartRef.current !== null && chartRef.current !== undefined) {
      //get current chart info
      const chart = chartRef.current.chart;
      const currentMinX = chart.toolbar.minX;
      const currentMaxX = chart.toolbar.maxX;
      const totalX = currentMaxX - currentMinX;

      // Calculate the relative position of the mouse on the chart
      var chartRect = chart.w.globals.dom.baseEl.getBoundingClientRect();
      var mouseX = (event.clientX - chartRect.left) / chartRect.width;

      // Determine zoom factor
      const zoomFactorIn = 0.7;
      const zoomFactorOut = 1;
      let zoomRange;
      let newMinX: number, newMaxX: number;

      if (event.deltaY < 0) {
        //zoom in
        zoomRange = zoomFactorIn * totalX;
        //used to zoom in based on where is mouse palced on screen
        var midPoint = currentMinX + mouseX * totalX;
        newMinX = midPoint - zoomRange / 2;
        newMaxX = midPoint + zoomRange / 2;
      } else {
        //zoom out
        zoomRange = zoomFactorOut * totalX;
        newMinX = currentMinX - zoomRange / 2;
        newMaxX = currentMaxX + zoomRange / 2;
      }

      // Constrain within original chart bounds
      newMinX = Math.max(newMinX, chart.w.globals.initialMinX);
      newMaxX = Math.min(newMaxX, chart.w.globals.initialMaxX);

      // Apply zoom if valid
      if (!isNaN(newMinX) && !isNaN(newMaxX) && newMinX < newMaxX) {
        chart.zoomX(newMinX, newMaxX);
      }
    }
  };

  return {
    scrollToZoom,
    chartRef,
  };
};
