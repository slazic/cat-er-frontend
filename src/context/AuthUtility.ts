import jwtDecode from "jwt-decode";

type Jwt = string | null | undefined;

type JwtClaims = {
  role: string;
  sub: string;
  iss: string;
  iat: number;
  exp: number;
};

const extractJwtClaims = (token: Jwt) => {
  let claims: JwtClaims | undefined;

  if (token) {
    claims = jwtDecode(token);
  }

  return claims;
};

const extractUserRole = (token: Jwt) => {
  let userRole: string | undefined;

  if (token) {
    userRole = extractJwtClaims(token)?.role;
  }

  return userRole;
};

const extractSubject = (token: Jwt) => {
  let subject: string | undefined;

  if (token) {
    subject = extractJwtClaims(token)?.sub;
  }

  return subject;
};

export { type Jwt, type JwtClaims };
export { extractJwtClaims, extractUserRole, extractSubject };
