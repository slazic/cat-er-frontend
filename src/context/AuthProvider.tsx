import React, { createContext, FC, ReactNode, useState } from "react";

import { Jwt } from "./AuthUtility";

type AuthContextProps = {
  token: Jwt;
  saveToken: (newState?: Jwt) => void;
};

const AuthContext = createContext<AuthContextProps>({
  token: undefined,
  saveToken: () => {},
});

const AuthProvider: FC<{ children: ReactNode }> = ({ children }) => {
  const [token, setToken] = useState<string | undefined | null>(sessionStorage.getItem("token"));

  const saveToken = (newState?: Jwt): void => {
    if (newState) {
      sessionStorage.setItem("token", newState);
      setToken(newState);
    } else {
      sessionStorage.removeItem("token");
      setToken(undefined);
    }
  };

  return <AuthContext.Provider value={{ token: token, saveToken: saveToken }}>{children}</AuthContext.Provider>;
};

export default AuthProvider;
export { AuthContext };
