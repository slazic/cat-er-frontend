import { FC, useContext } from "react";
import { AuthContext } from "../context/AuthProvider";
import { Navigate, Outlet, useLocation } from "react-router-dom";
import { JwtClaims } from "../context/AuthUtility";
import jwtDecode from "jwt-decode";

interface ProtectedRouteProps {
  allowedRoles: string[];
}

const ProtectedRoute: FC<ProtectedRouteProps> = ({ allowedRoles }) => {
  const { token } = useContext(AuthContext);
  const location = useLocation();
  let claims: JwtClaims | undefined;

  if (token) {
    claims = jwtDecode(token);
  }

  return claims?.role ? (
    allowedRoles.includes(claims?.role) ? (
      <Outlet />
    ) : (
      <Navigate to={"/unauthorized"} state={{ from: location }} replace />
    )
  ) : (
    <Navigate to={"/sign_in"} state={{ from: location }} replace />
  );
};

export default ProtectedRoute;
