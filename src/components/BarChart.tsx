import { ApexOptions } from "apexcharts";
import { type } from "os";
import Chart from "react-apexcharts";

const chartOptions: ApexOptions = {
  chart: {
    type: "bar",
    height: 350,
  },
  colors: ["#e87e05"],
  title: {
    text: "Visitors",
    align: "left",
  },
  xaxis: {
    categories: ["Binance", "Coinbase", "Bittrex", "Bitfinex", "Kraken"],
    labels: {
      style: {
        fontSize: "14px",
      },
    },
  },
  yaxis: {
    tooltip: {
      enabled: true,
    },
    labels: {
      style: {
        fontSize: "14px",
      },
    },
  },
  plotOptions: {
    bar: {
      borderRadius: 5,
      horizontal: true,
    },
  },
};

const series: ApexOptions["series"] = [
  {
    data: [32359110, 23315031, 7010204, 4458020, 2266011],
  },
];

const BarChart = () => {
  return (
    <Chart
      options={chartOptions}
      series={series}
      type={chartOptions.chart?.type}
    />
  );
};

export default BarChart;
