const Footer = () => {
  return (
    <>
      <div className="bg-neutral-800">
        <div className="px-4 py-4 xl:px-12 xl:py-7 2xl:px-24 2xl:py-14">
          <div className="mb-4 lg:mb-14">
            <div className="sm:w-1/2 md:w-1/2 2xl:w-1/4">
              <img
                src="./ferit.png"
                className="w-4/5 sm:w-full md:w-full lg:w-2/3 xl:w-2/3 2xl:w-full"
                title="FERIT"
                alt="FERIT-logo"
              />
            </div>
            <div className="sm:w-1/2 md:w-1/2 lg:w-1/3 2xl:w-1/4">
              <img
                src="./white-cat.png"
                className="w-1/5 mt-4 sm:w-1/4 md:w-1/4 xl:w-1/7"
                title="CAT-ER"
                alt="CAT-ER-logo"
              />
            </div>
          </div>
          <div className="grid gap-y-4 md:gap-y-0 md:grid-cols-2">
            <div className="grid justify-items-start text-white text-landing">
              <div>
                Kneza Trpimira 2B, HR-31000 Osijek | Cara Hadrijana 10b,
                HR-31000 Osijek
              </div>
            </div>
            <div>
              <div className="grid md:justify-items-end text-white text-landing">
                <div>Tel: +385 (0) 31 224-600 | Fax: +385 (0) 31 224-605</div>
              </div>
            </div>
          </div>
          <div className="mt-4 text-white text-landing text-start md:text-end">
            © 2024 CATER | lazicc9@gmail.com
          </div>
        </div>
        <div className="bg-amber-600 h-24"></div>
        <div className="w-full bg-neutral-800 h-8"></div>
      </div>
    </>
  );
};

export default Footer;
