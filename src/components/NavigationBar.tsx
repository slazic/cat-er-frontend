import { useContext, useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { AuthContext } from "../context/AuthProvider";
import { signOut } from "../api/authentication";
import { toast } from "react-toastify";
import { extractUserRole } from "../context/AuthUtility";

export type ActiveLink = {
  isActive: boolean;
  isPending: boolean;
};

const NavigationBar = () => {
  const [isHovered, setIsHovered] = useState(false);
  const { token, saveToken } = useContext(AuthContext);
  const navigate = useNavigate();

  const navLinkStye = ({ isActive, isPending }: ActiveLink) =>
    isActive
      ? "font-bold text-base text-white px-4"
      : "font-bold text-base text-neutral-700 px-4 ease-in-out duration-300 hover:text-white hover:text-lg";

  const toggleHover = (value: boolean) => {
    setIsHovered(value);
  };

  const onSignOut = async () => {
    let status: number | string | undefined;

    await signOut()
      .then((response) => {
        status = response.status;
        toast.success("Successful logout.", {
          position: "top-center",
          autoClose: 3000,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
        });
      })
      .catch((errors) => {
        console.log(errors);
      });

    if (status && status === 200) {
      if (saveToken) saveToken();
      navigate("/");
    }
  };

  return (
    <>
      <div className="w-full bg-neutral-800 h-8"></div>
      <nav
        className="bg-amber-600 h-24 flex items-center"
        style={{ fontFamily: "HelveticaNeue-Medium" }}
      >
        <div className="container mx-auto px-2 flex justify-between">
          <ul>
            <li>
              <NavLink
                to={"/"}
                onMouseEnter={() => toggleHover(true)}
                onMouseLeave={() => toggleHover(false)}
              >
                {({ isActive }) => (
                  <img
                    src={
                      isActive
                        ? "./white-cat.png"
                        : isHovered
                        ? "./white-cat.png"
                        : "./black-cat.png"
                    }
                    className={`${
                      isHovered
                        ? isActive
                          ? "h-16"
                          : "h-20 ease-in-out duration-300"
                        : "h-16 ease-in-out duration-300"
                    }`}
                    alt=""
                  />
                )}
              </NavLink>
            </li>
          </ul>
          <ul className="flex items-center">
            {token ? (
              <>
                {extractUserRole(token) === "ADMIN" && (
                  <li className="">
                    <NavLink to={"/dashboard"} className={navLinkStye}>
                      DASHBOARD
                    </NavLink>
                  </li>
                )}
                <li className="">
                  <NavLink to={"/profile"} className={navLinkStye}>
                    PROFILE
                  </NavLink>
                </li>
                <li className="">
                  <button
                    className="font-bold text-base text-neutral-700 px-4 ease-in-out duration-300 hover:text-white hover:text-lg"
                    onClick={() => onSignOut()}
                  >
                    SIGN OUT
                  </button>
                </li>
              </>
            ) : (
              <li className="">
                <NavLink to={"/sign_in"} className={navLinkStye}>
                  SIGN IN
                </NavLink>
              </li>
            )}
          </ul>
        </div>
      </nav>
    </>
  );
};

export default NavigationBar;
