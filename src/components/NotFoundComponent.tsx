import React from "react";
import { Link, useLocation } from "react-router-dom";

const NotFoundComponent = (): JSX.Element => {
  const location = useLocation();
  const from = location.state?.from?.pathname || "/";

  return (
    <>
      <div className="">
        <p className="text-7xl">404</p>
        <p className="text-4xl">Sorry!</p>
        <p className="text-lg">
          The data you're looking for, does not exist. <br />
          Please check your search parameters or contact administrator for
          details.
        </p>
        <Link to={from} className="underline text-blue-500 text-lg ms-2">
          Go back.
        </Link>
      </div>
    </>
  );
};

export default NotFoundComponent;
