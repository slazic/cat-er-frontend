import { ApexOptions } from "apexcharts";
import { useEffect } from "react";
import Chart from "react-apexcharts";
import Loader from "./Loader";
import { isEmptyArray } from "formik";
import NotFoundComponent from "./NotFoundComponent";
import { useDetails } from "../hooks/useDetails";

interface CandlestickChartProps {
  chartInterval: string | undefined;
  choosenMarket: string | undefined;
  chartData: any;
  currencyDetails: any;
  loading: boolean;
}

const CandlestickChart: React.FC<CandlestickChartProps> = ({
  chartInterval,
  choosenMarket,
  chartData,
  currencyDetails,
  loading,
}): JSX.Element => {
  const { scrollToZoom, chartRef } = useDetails();

  useEffect(() => {
    const chartElement = document.querySelector("#zoom_in_div");

    if (chartElement) {
      chartElement.addEventListener("wheel", scrollToZoom, {
        passive: false,
      });

      return () => {
        chartElement.removeEventListener("wheel", scrollToZoom);
      };
    }
  }, [scrollToZoom]);

  const series: ApexOptions["series"] = [
    {
      data: chartData.data,
    },
  ];

  const chartOptions: ApexOptions = {
    chart: {
      type: "candlestick",
      toolbar: {
        show: false,
        tools: {
          selection: false,
          zoom: false,
          reset: false,
          download: false,
          zoomin: false,
          zoomout: false,
          pan: false,
        },
        //autoSelected: "pan",
      },
      selection: {
        enabled: false,
      },
      events: {
        //scrolled u smislu kada koristis 'pan' i vuces graf levo ili desno
        scrolled: function (chartContext, { xaxis }) {},

        //pokrene se na selection zoom ili klikom na zoom in/out button
        zoomed: function (chartContext, { xaxis, yaxis }) {},
        brushScrolled: function (chartContext, { xaxis, yaxis }) {},
      },
    },
    xaxis: {
      type: "datetime",
      tooltip: {
        enabled: true,
        formatter: function (val: string, options: any) {
          return new Date(val)
            .toLocaleString("en-GB", {
              year: "numeric",
              month: "numeric",
              day: "numeric",
              hour: "numeric",
              minute: "numeric",
            })
            .replace(/\//g, ".");
        },
      },
    },
    yaxis: {
      tooltip: {
        enabled: true,
      },
      opposite: true,
    },
    tooltip: {
      enabled: true,
      followCursor: true,
      theme: "dark",
      fixed: {
        enabled: true,
        position: "topLeft",
        offsetX: 25,
        offsetY: 25,
      },
    },
    grid: {
      show: true,
      strokeDashArray: 10,
      xaxis: {
        lines: {
          show: true,
        },
      },
      yaxis: {
        lines: {
          show: true,
        },
      },
    },
  };

  return (
    <>
      {loading ? (
        <div
          style={{
            height: "calc(100vh - 48px - 32px - 8px)",
            maxHeight: "calc(100vh - 48px - 32px - 8px)",
            minHeight: "calc(100vh - 48px - 32px - 8px)",
          }}
        >
          <div className="flex justify-center items-center h-full">
            <div>
              <Loader />
            </div>
          </div>
        </div>
      ) : isEmptyArray(chartData) ? (
        <NotFoundComponent />
      ) : (
        <>
          <div className="flex items-center pt-2 ps-2 cursor-default select-none">
            <img
              src={currencyDetails?.logoUrl}
              style={{ height: 24, width: 24, marginRight: 10 }}
              alt="curreny-logo"
            />
            <div>
              {currencyDetails?.name +
                " - " +
                chartInterval +
                " - " +
                choosenMarket}
            </div>
          </div>
          <div
            id="zoom_in_div"
            className="active:cursor-grabbing hover:cursor-crosshair"
            style={{
              height: "calc(100vh - 48px - 32px - 8px)",
              maxHeight: "calc(100vh - 48px - 32px - 8px)",
              minHeight: "calc(100vh - 48px - 32px - 8px)",
            }}
          >
            <Chart
              ref={chartRef}
              options={chartOptions}
              series={series}
              type={chartOptions.chart?.type}
              height={"98%"}
            />
          </div>
        </>
      )}
    </>
  );
};

export default CandlestickChart;
