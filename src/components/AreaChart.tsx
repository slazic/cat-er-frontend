import { ApexOptions } from "apexcharts";
import Chart from "react-apexcharts";

interface AreaChartProps {
  data: [];
  color: string;
}

const AreaChart: React.FC<AreaChartProps> = ({ data, color }) => {
  const series: ApexOptions["series"] = [
    {
      data: data,
    },
  ];

  const chartOptions: ApexOptions = {
    chart: {
      type: "area",
      toolbar: {
        show: false,
      },
      zoom: {
        enabled: false,
      },
      selection: {
        enabled: false,
      },
    },
    stroke: {
      curve: "straight",
      width: 1,
    },
    grid: {
      show: false,
    },
    dataLabels: {
      enabled: false,
    },
    tooltip: {
      enabled: false,
    },
    xaxis: {
      type: "datetime",
      labels: {
        show: false,
      },
      axisBorder: {
        show: false,
      },
      axisTicks: {
        show: false,
      },
    },
    yaxis: {
      labels: {
        show: false,
      },
      axisBorder: {
        show: false,
      },
      axisTicks: {
        show: false,
      },
    },
    colors: [color],
    fill: {
      type: "gradient",
      gradient: {
        shadeIntensity: 1,
        opacityFrom: 0.7,
        opacityTo: 0.9,
        stops: [0, 100],
      },
    },
  };

  return (
    <Chart
      options={chartOptions}
      series={series}
      type={chartOptions.chart?.type}
      height={100}
    />
  );
};

export default AreaChart;
