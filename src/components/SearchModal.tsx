import { faCaretDown, faCaretUp, faMagnifyingGlass, faXmark } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import { cryptoList } from "./homepage/MostPopularList";
import { cryptoExchanges } from "../pages/ChartPage";

export interface SearchModalProps {
  isSearchOpen: boolean;
  setIsSearchOpen: React.Dispatch<React.SetStateAction<boolean>>;
  searchFieldRef: React.RefObject<HTMLInputElement>;
  setChoosenMarket: React.Dispatch<React.SetStateAction<string>>;
  setChoosenTicker: React.Dispatch<React.SetStateAction<string>>;
}

const SearchModal: React.FC<SearchModalProps> = ({
  isSearchOpen,
  setIsSearchOpen,
  searchFieldRef,
  setChoosenMarket,
  setChoosenTicker,
}): JSX.Element => {
  const [isSortClicked, setIsSortClicked] = useState<boolean>(false);

  const [searchTerm, setSearchTerm] = useState<string>("");

  const handleSearch = (event: any) => {
    setSearchTerm(event.target.value.toLowerCase());
  };

  return (
    <>
      <div
        className={
          isSearchOpen
            ? "z-10 w-full h-full absolute bg-black bg-opacity-25 flex justify-center items-center p-5 2xl:p-0"
            : "hidden"
        }
        onClick={() => {
          setIsSearchOpen(false);
        }}
      >
        <div
          className={
            isSearchOpen ? "bg-white 2xl:w-1/2 2xl:h-2/3 w-full h-3/4 rounded-lg shadow-lg select-none" : "hidden"
          }
          onClick={(e) => {
            e.stopPropagation();
          }}
        >
          <div className="flex flex-col h-full w-full justify-between">
            <div id="header" className="">
              <div className="flex justify-between px-5 py-4">
                <div id="title" className="text-xl font-semibold cursor-default">
                  Seek for your favourite tickers
                </div>
                <div id="exit">
                  <FontAwesomeIcon
                    icon={faXmark}
                    size="xl"
                    className="hover:text-gray-400 ease-in duration-200 cursor-pointer"
                    onClick={() => {
                      setIsSearchOpen(false);
                    }}
                  />
                </div>
              </div>
              <div id="search-bar" className="border border-x-0 ps-5 py-2 flex justify-between w-full">
                <div className="">
                  <FontAwesomeIcon icon={faMagnifyingGlass} />
                </div>
                <div className="ms-2 w-full">
                  <input
                    type="text"
                    placeholder="Search..."
                    className="w-full border-none outline-none bg-transparent"
                    ref={searchFieldRef}
                    onChange={handleSearch}
                  />
                </div>
              </div>
              <div id="categories" className="ps-5 py-2">
                <button className="rounded-full bg-black text-white text-sm py-2 px-5 text-center font-semibold">
                  All
                </button>
              </div>
              <div className="ps-5 pb-2">
                <div className="cursor-default flex space-x-5">
                  <div
                    className="flex w-fit hover:bg-gray-100 px-5 py-2 rounded-md text-sm font-semibold"
                    onClick={() => {
                      setIsSortClicked(!isSortClicked);
                    }}
                  >
                    <div>All markets</div>
                    <div className="ms-2">
                      <FontAwesomeIcon icon={isSortClicked ? faCaretUp : faCaretDown} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div id="body" className="overflow-auto h-full w-full">
              <div id="list" className="">
                <table className="h-full w-full">
                  <tbody>
                    {cryptoList
                      .filter(
                        (crypto) =>
                          crypto.ticker.toLowerCase().includes(searchTerm) ||
                          crypto.name.toLowerCase().includes(searchTerm)
                      )
                      .map((crypto, index) => {
                        return cryptoExchanges.map((exchangeName, index) => {
                          return (
                            <tr
                              className="border-b hover:bg-gray-100 cursor-default"
                              onClick={() => {
                                setChoosenTicker(crypto.ticker);
                                setChoosenMarket(exchangeName.name);
                                setIsSearchOpen(false);
                              }}
                            >
                              <td className="ps-5 py-2">
                                <div className="flex items-center">
                                  <div>
                                    <img src={crypto.logoUrl} className="h-5" alt="curreny-logo" />
                                  </div>
                                  <div className="ms-3">
                                    {crypto.ticker.toLowerCase().includes(searchTerm) ? (
                                      <>
                                        {crypto.ticker.slice(0, crypto.ticker.toLowerCase().indexOf(searchTerm))}
                                        <span className="font-bold text-blue-500">
                                          {crypto.ticker.slice(
                                            crypto.ticker.toLowerCase().indexOf(searchTerm),
                                            crypto.ticker.toLowerCase().indexOf(searchTerm) + searchTerm.length
                                          )}
                                        </span>
                                        {crypto.ticker.slice(
                                          crypto.ticker.toLowerCase().indexOf(searchTerm) + searchTerm.length
                                        )}
                                      </>
                                    ) : (
                                      crypto.ticker
                                    )}
                                  </div>
                                </div>
                              </td>
                              <td>
                                <div>
                                  {crypto.name.toLowerCase().includes(searchTerm) ? (
                                    <>
                                      {crypto.name.slice(0, crypto.name.toLowerCase().indexOf(searchTerm))}
                                      <span className="font-bold text-blue-500">
                                        {crypto.name.slice(
                                          crypto.name.toLowerCase().indexOf(searchTerm),
                                          crypto.name.toLowerCase().indexOf(searchTerm) + searchTerm.length
                                        )}
                                      </span>
                                      {crypto.name.slice(
                                        crypto.name.toLowerCase().indexOf(searchTerm) + searchTerm.length
                                      )}
                                    </>
                                  ) : (
                                    crypto.name
                                  )}
                                </div>
                              </td>
                              <td className="pe-5 flex justify-end">
                                <div className="flex items-center" style={{ width: "90px" }}>
                                  <div>
                                    <img src={exchangeName.logoUrl} alt={`${exchangeName.name}-logo`} className="h-5" />
                                  </div>
                                  <div className="ms-3">{exchangeName.name}</div>
                                </div>
                              </td>
                            </tr>
                          );
                        });
                      })}
                  </tbody>
                </table>
              </div>
            </div>
            <div id="footer" className="px-5 py-3 bg-gray-100 rounded-b-lg text-center text-gray-500 cursor-default">
              Simply start typing to open this modal...
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default SearchModal;
