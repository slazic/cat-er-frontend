import { faCaretDown, faCaretUp } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { FC } from "react";

interface ExchangesCardProps {
  imgSrc: string;
  name: string;
  currentPrice: number;
  changeValue: number;
  changePercentage: number;
  toggler: boolean;
}

const ExchangesCard: FC<ExchangesCardProps> = ({
  imgSrc,
  name,
  currentPrice,
  changeValue,
  changePercentage,
  toggler,
}) => {
  return (
    <div className="grid grid-cols-3 text-exchange-name p-2 text-center">
      <div className="flex items-center">
        <img src={imgSrc} className="w-12"></img>
        <div className="ms-2">{name}</div>
      </div>
      <div className="blur-sm">$ {currentPrice}</div>
      <div
        className={`flex items-center justify-center text-white rounded-lg p-3 ${
          toggler ? "bg-green-600" : "bg-red-700"
        }`}
      >
        <div className="blur-sm">
          {changePercentage}% / $ {changeValue}
        </div>
        <div className="ms-3">
          <FontAwesomeIcon icon={toggler ? faCaretDown : faCaretUp} size="lg" />
        </div>
      </div>
    </div>
  );
};

export default ExchangesCard;
