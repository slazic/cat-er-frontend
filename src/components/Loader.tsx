const Loader = () => {
  return (
    <div className="h-10 w-10 animate-spin rounded-full border-4 border-solid border-current border-r-transparent text-amber-600"></div>
  );
};

export default Loader;
