import { useContext, useEffect, useState } from "react";
import AreaChart from "../AreaChart";
import Loader from "../Loader";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import axios from "axios";
import { AuthContext } from "../../context/AuthProvider";

const BASE_URL = "http://localhost:8080/api/v1";

interface Binance5MinIntervalResponse {
  data: any[];
}

export interface CryptoData {
  name: string;
  ticker: string;
  logoUrl: string;
}

export const cryptoList: CryptoData[] = [
  { name: "Bitcoin", ticker: "BTCEUR", logoUrl: "./market_logo/bitcoin.png" },
  { name: "Ethereum", ticker: "ETHEUR", logoUrl: "./market_logo/eth.png" },
  { name: "Ripple", ticker: "XRPEUR", logoUrl: "./market_logo/xrp.png" },
  { name: "Cardano", ticker: "ADAEUR", logoUrl: "./market_logo/cardano.png" },
  { name: "Litecoin", ticker: "LTCEUR", logoUrl: "./market_logo/ltcoin.png" },
];

const MostPopularList = () => {
  const { token } = useContext(AuthContext);

  const [series, setSeries] = useState<Binance5MinIntervalResponse>({
    data: [],
  });

  const [loading, setLoading] = useState(true);
  const navigate = useNavigate();

  useEffect(() => {
    const fetchData = async () => {
      await axios
        .get(BASE_URL + "/fetchExternal/binance5MinInterval?symbols=BTCEUR,ETHEUR,XRPEUR,ADAEUR,LTCEUR", {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((response) => {
          setSeries({ data: response.data });
        })
        .catch((error) => {
          toast.error(error.response.data.message, {
            position: "top-center",
            autoClose: 3000,
            closeOnClick: true,
            pauseOnHover: false,
            draggable: true,
          });
        })
        .finally(() => {
          setLoading(false);
        });
    };

    fetchData();
  }, []);

  return (
    <div className="bg-neutral-100 2xl:py-16 grid sm:grid-cols-2 md:grid-cols-3 justify-items-center 2xl:grid-cols-5 ">
      {Array.from({ length: 5 }).map((_, index) => (
        <div
          key={index}
          className="my-4 w-64 h-64 bg-white rounded-lg shadow ease-in-out duration-150 hover:scale-110"
          onClick={() => {
            navigate("/chart", {
              state: { ticker: cryptoList[index].ticker },
            });
          }}
        >
          {loading ? (
            <Loader />
          ) : series.data[index][0][1] <
            series.data[index][series.data.length - 1][1] ? (
            <>
              <div className="flex pl-6 pt-6">
                <div>
                  <img
                    src={cryptoList[index].logoUrl}
                    style={{ width: 44, height: 44, marginRight: 10 }}
                    alt="crypto-logo"
                  />
                </div>
                <div>
                  <p>{cryptoList[index].name}</p>
                  <p className="text-neutral-400">{cryptoList[index].ticker}</p>
                </div>
              </div>
              <AreaChart data={series.data[index]} color="#16A34A" />
              <div className="pb-6 pl-6">
                <p className="text-green-600">
                  +
                  {(
                    ((series.data[index][series.data.length - 1][1] -
                      series.data[index][0][1]) /
                      series.data[index][0][1]) *
                    100
                  ).toFixed(2)}
                  %
                </p>
                <p className="font-bold text-black text-xl">
                  €{series.data[index][series.data.length - 1][1].toFixed(2)}
                </p>
              </div>
            </>
          ) : (
            <>
              <div className="flex pl-6 pt-6">
                <div>
                  <img
                    src={cryptoList[index].logoUrl}
                    style={{ width: 44, height: 44, marginRight: 10 }}
                    alt="crypto-logo"
                  />
                </div>
                <div>
                  <p>{cryptoList[index].name}</p>
                  <p className="text-neutral-400">{cryptoList[index].ticker}</p>
                </div>
              </div>
              <AreaChart data={series.data[index]} color="#DC2626" />
              <div className="pl-6 pb-6">
                <p className="text-red-600">
                  {(
                    ((series.data[index][series.data.length - 1][1] -
                      series.data[index][0][1]) /
                      series.data[index][0][1]) *
                    100
                  ).toFixed(2)}
                  %
                </p>
                <p className="font-bold text-black text-xl">
                  €{series.data[index][series.data.length - 1][1].toFixed(2)}
                </p>
              </div>
            </>
          )}
        </div>
      ))}
    </div>
  );
};

export default MostPopularList;
