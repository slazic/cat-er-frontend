import { useEffect, useRef, useState } from "react";
import { Tooltip } from "react-tooltip";

interface CustomDropdownProps {
  chartInterval: string;
  setChartInterval: (value: string) => void;
}

const CustomDropdown: React.FC<CustomDropdownProps> = ({
  chartInterval,
  setChartInterval,
}): JSX.Element => {
  const [isOpen, setIsOpen] = useState(false);
  const dropdownRef = useRef<HTMLDivElement>(null);

  const handleOptionClick = (value: string) => {
    setChartInterval(value);
    setIsOpen(false);
  };

  const getIntervalText = (interval: string): string => {
    switch (chartInterval) {
      case "1m":
        return "1 minute";
      case "1h":
        return "1 hour";
      case "D":
        return "1 day";
      case "W":
        return "1 week";
      case "M":
        return "1 month";
      default:
        return "1 day";
    }
  };

  useEffect(() => {
    const handleClickOutside = (event: any) => {
      if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
        setIsOpen(false);
      }
    };

    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, []);

  return (
    <div className="relative inline-block" ref={dropdownRef}>
      <button
        id="chart-interval"
        className="hover:bg-gray-100 py-2 px-4 rounded-md mx-2 cursor-pointer text-center"
        onClick={() => setIsOpen(!isOpen)}
      >
        {chartInterval}
      </button>
      <Tooltip
        anchorSelect="#chart-interval"
        content={getIntervalText(chartInterval)}
        place="bottom"
      />

      {isOpen && (
        <div className="absolute z-20 w-56 bg-white border-2 shadow-md translate-y-1 py-2 rounded-md">
          <div className="px-2 text-sm text-gray-400">MINUTES</div>
          <button
            className="block hover:bg-gray-100 w-full text-left py-2 px-4 text-sm"
            onClick={() => {
              handleOptionClick("1m");
            }}
          >
            1 minute
          </button>
          <div className="px-2 text-sm text-gray-400">HOURS</div>
          <button
            className="block hover:bg-gray-100 w-full text-left py-2 px-4 text-sm"
            onClick={() => {
              handleOptionClick("1h");
            }}
          >
            1 hour
          </button>
          <div className="px-2 text-sm text-gray-400">DAYS</div>
          <button
            className="block hover:bg-gray-100 w-full text-left  py-2 px-4 text-sm"
            onClick={() => {
              handleOptionClick("D");
            }}
          >
            1 day
          </button>
          <div className="px-2 text-sm text-gray-400">WEEKS</div>
          <button
            className="block hover:bg-gray-100 w-full text-left py-2 px-4 text-sm"
            onClick={() => {
              handleOptionClick("W");
            }}
          >
            1 week
          </button>
          <div className="px-2 text-sm text-gray-400">MONTHS</div>
          <button
            className="block hover:bg-gray-100 w-full text-left py-2 px-4 text-sm"
            onClick={() => {
              handleOptionClick("M");
            }}
          >
            1 month
          </button>
        </div>
      )}
    </div>
  );
};

export default CustomDropdown;
