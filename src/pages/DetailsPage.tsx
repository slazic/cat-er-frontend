import React, { useEffect, useState } from "react";
import NavigationBar from "../components/NavigationBar";
import Footer from "../components/Footer";
import CandlestickChart from "../components/CandlestickChart";

const DetailsPage = (): JSX.Element => {
  return (
    <>
      <NavigationBar />
      <div className="w-3/4">
        <CandlestickChart />
      </div>
      <Footer />
    </>
  );
};

export default DetailsPage;
