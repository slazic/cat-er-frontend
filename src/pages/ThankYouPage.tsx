import React from "react";
import { Link } from "react-router-dom";

const ThankYouPage = () => {
  return (
    <>
      <div className="xl:container xl:m-auto xl:px-44">
        <div className="pt-24 xl:shadow-lg xl:mt-10 xl:pt-10 xl:pb-8">
          <div className="text-center mb-10">
            <div className="text-4xl font-bold">Successfully registered!</div>
          </div>
          <div className="text-lg px-10 text-justify">
            <p className="pb-3">Thank you for your registration.</p>
            The verification link has been sent to the email address you used
            during registration.
            <br /> Please confirm your email address by clicking the link in
            order to successfully log in.
            <p className="pt-3 underline text-blue-500">
              The link will expire after 24 hours.
            </p>
          </div>
          <div className="px-24">
            <hr className="my-8"></hr>
          </div>
          <div className="text-center">
            <Link to="/sign_in">
              <button
                type="button"
                className="rounded-md bg-amber-600 text-white text-base font-bold py-3 w-2/3 hover:xl:shadow-lg xl:w-3/12"
              >
                Sign In
              </button>
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};

export default ThankYouPage;
