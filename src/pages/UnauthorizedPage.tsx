import { replace } from "formik";
import { Link, useLocation } from "react-router-dom";
import NavigationBar from "../components/NavigationBar";
import Footer from "../components/Footer";

const UnauthorizedPage = () => {
  const location = useLocation();
  const from = location.state?.from?.pathname || "/";

  return (
    <>
      <NavigationBar />
      <div className="">
        <p className="text-7xl">401</p>
        <p className="text-4xl">Sorry!</p>
        <p className="text-lg">
          You are not authorized to access this page. <br />
          Please check your login credentials or contact administrator for
          access.
        </p>
        <Link to={from} className="underline text-blue-500 text-lg ms-2">
          Go back.
        </Link>
      </div>
      <Footer />
    </>
  );
};

export default UnauthorizedPage;
