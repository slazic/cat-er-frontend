import SignUpForm from "../forms/SignUpForm";
import { Link } from "react-router-dom";

const SignUpPage = () => {
  return (
    <>
      <div className="xl:container xl:m-auto xl:px-44">
        <div className="pt-24 xl:shadow-lg xl:mt-10 xl:pt-10 xl:pb-8">
          <div className="text-center mb-10">
            <div className="text-2xl">Hey there,</div>
            <div className="text-4xl font-bold">Create an account.</div>
          </div>
          <SignUpForm />
          <div className="px-24">
            <hr className="mb-8"></hr>
          </div>
          <div className="text-center">
            Already have an account?
            <Link
              to="/sign_in"
              className="underline text-blue-500 ms-2"
              replace={true}
            >
              Sign In
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};

export default SignUpPage;
