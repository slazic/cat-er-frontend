import { useContext } from "react";

import { AuthContext } from "../context/AuthProvider";
import NavigationBar from "../components/NavigationBar";
import Footer from "../components/Footer";
import MostPopularList from "../components/homepage/MostPopularList";
import { ToastContainer } from "react-toastify";

const HomePage = () => {
  const { token } = useContext(AuthContext);

  return token ? (
    <>
      <NavigationBar />
      <div className="">
        <div className="px-4 2xl:px-96">
          <p className="text-light text-amber-600 text-lg mb-6 mt-4">
            Crypto Prices
          </p>
          <p className="text-medium text-black text-lg xl:text-2xl text-justify">
            The global cryptocurrency market cap today is{" "}
            <span className="text-amber-600">€1.25T</span>, a{" "}
            <span className="text-red-600">-0.18%</span> change from 24 hours
            ago.
          </p>
          <div className="flex justify-center mt-10">
            <div className="border-b-2 border-amber-600 rounded-sm text-2xl text-amber-600 w-fit">
              Most popular
            </div>
          </div>
        </div>
        <MostPopularList />
      </div>
      <Footer />
      <ToastContainer />
    </>
  ) : (
    <>
      <NavigationBar />
      <div className="bg-neutral-800">
        <div className="px-4 py-4 xl:py-24 2xl:pt-24 2xl:py-0 2xl:pb-14 2xl:px-16">
          <div className="xl:flex md:grid md:grid-rows-2 md:justify-items-center">
            <div className="xl:w-1/2">
              <span className="text-landing text-white text-7xl">
                Crypto Analysis Tool
              </span>{" "}
              <br />{" "}
              <span className="text-amber-600 text-landing text-4xl sm:text-6xl md:text-7xl 2xl:ms-10">
                Cut away time-waste
              </span>
              <p className="text-white text-landing sm:text-2xl lg:text-justify pt-16 pb-2">
                Tired of juggling between multiple exchanges to track crypto
                prices?
              </p>
              <p className="text-white text-landing sm:text-2xl text-justify xl:me-6 2xl:me-10">
                CAT brings all your favorite cryptocurrencies from different
                exchanges to one user-friendly platform. Say goodbye to the
                hassle of switching tabs and stay in control.
              </p>
            </div>
            <div className="py-4 md:w-2/3 lg:w-2/4 xl:w-1/2 xl:py-0">
              <img src="./search.png" alt="" />
            </div>
          </div>
        </div>
      </div>
      <Footer />
      <ToastContainer />
    </>
  );
};

export default HomePage;
