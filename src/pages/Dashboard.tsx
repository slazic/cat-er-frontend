import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Footer from "../components/Footer";
import NavigationBar from "../components/NavigationBar";
import { faLock, faUnlock } from "@fortawesome/free-solid-svg-icons";
import { Tooltip } from "react-tooltip";
import { useContext, useEffect, useState } from "react";
import { changeUserAccess, fetchAllUsers } from "../api/users";
import Loader from "../components/Loader";
import { toast, ToastContainer } from "react-toastify";
import { AuthContext } from "../context/AuthProvider";

const Dashboard: React.FC = (): JSX.Element => {
  const [iconHovered, setIconHovered] = useState(null);
  const [loading, setLoading] = useState(false);
  const [users, setUsers] = useState<any[]>([]);
  const { token } = useContext(AuthContext);

  const fetchAllUsersMethod = async () => {
    setLoading(true);
    await fetchAllUsers(token)
      .then((response: any) => {
        setUsers(response.data);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const handleOnStatusClick = async (userId: number, newAccessStatus: boolean) => {
    setLoading(true);
    await changeUserAccess(userId, newAccessStatus, token)
      .then((response) => {
        fetchAllUsersMethod();
        toast.success(response.data, {
          position: "top-center",
          autoClose: 3000,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
        });
      })
      .catch((error) => {
        toast.error(error.data, {
          position: "top-center",
          autoClose: 3000,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
        });
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    fetchAllUsersMethod();
  }, []);

  return (
    <>
      <NavigationBar />
      <div className="p-5 flex justify-center">
        <div className="w-1/2 shadow-2xl rounded-md">
          {loading ? (
            <Loader />
          ) : (
            <table className="w-full">
              <thead className="">
                <tr className="border-b">
                  <th className="py-5 px-3 text-left">Email</th>
                  <th className="py-5 px-3">Status</th>
                  <th className="py-5 px-3 text-right">Actions</th>
                </tr>
              </thead>
              <tbody>
                {users.map((user) => (
                  <tr key={user.id} className="">
                    <td className="py-2 px-3">{user.email}</td>
                    <td className="text-center">{user.locked ? "Locked" : "Unlocked"}</td>
                    <td className="py-2 px-3 flex justify-end items-center">
                      <FontAwesomeIcon
                        id={`action-${user.id}`}
                        icon={
                          iconHovered === user.id ? (user.locked ? faUnlock : faLock) : user.locked ? faLock : faUnlock
                        }
                        className={`ease-in-out duration-300 text-${user.locked ? "red" : "green"}-600 hover:text-${
                          user.locked ? "green" : "red"
                        }-600`}
                        onClick={() => {
                          handleOnStatusClick(user.id, !user.locked);
                        }}
                        onMouseEnter={() => setIconHovered(user.id)}
                        onMouseLeave={() => setIconHovered(null)}
                      />
                      <Tooltip
                        anchorSelect={`#action-${user.id}`}
                        content={user.locked ? "Unlock" : "Lock"}
                        place="right"
                      />
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          )}
        </div>
      </div>
      <ToastContainer />
      <Footer />
    </>
  );
};

export default Dashboard;
