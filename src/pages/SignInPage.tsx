import React from "react";
import SignInForm from "../forms/SignInForm";
import { Link } from "react-router-dom";
import { ToastContainer } from "react-toastify";

const SignInPage = () => {
  return (
    <>
      <div className="xl:container xl:m-auto xl:px-44">
        <div className="pt-24 xl:shadow-lg xl:mt-10 xl:pt-10 xl:pb-8">
          <div className="text-center mb-10">
            <div className="text-2xl">Hey there,</div>
            <div className="text-4xl font-bold">Welcome back.</div>
          </div>
          <SignInForm />
          <div className="px-24">
            <hr className="mb-8"></hr>
          </div>
          <div className="text-center">
            Don't have an account?
            <Link to="/sign_up" className="underline text-blue-500 ms-2" replace={true}>
              Sign Up
            </Link>
          </div>
        </div>
      </div>
      <ToastContainer />
    </>
  );
};

export default SignInPage;
