import NavigationBar from "../components/NavigationBar";
import Footer from "../components/Footer";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown, faCaretUp, faPen, faUser } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import { Form, Field, ErrorMessage, FormikHelpers, Formik, FormikValues } from "formik";
import axios from "axios";
import * as Yup from "yup";
import { toast, ToastContainer } from "react-toastify";
import { AuthContext } from "../context/AuthProvider";

interface UserProfileResponse {
  id: number;
  firstName: string;
  lastName: string;
  username: string;
  createdAt: number[];
}

interface ChangePasswordFormValues {
  currentPassword: string;
  newPassword: string;
  confirmPassword: string;
}

interface ChangePasswordDto {
  oldPassword: string;
  newPassword: string;
}

const ChangePasswordSchema = Yup.object().shape({
  currentPassword: Yup.string()
    .min(8, "Password must contain at least 8 characters")
    .required("This field is required."),
  newPassword: Yup.string().min(8, "Password must contain at least 8 characters").required("This field is required."),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref("newPassword")], "Passwords must match")
    .required("This field is required."),
});

const ProfilePage = (): JSX.Element => {
  const [passwordDropdownClicked, setPasswordDropdownClicked] = useState(false);
  const [userData, setUserData] = useState<UserProfileResponse>({
    id: 0,
    firstName: "",
    lastName: "",
    username: "",
    createdAt: [],
  });
  const [createdAt, setCreatedAt] = useState("");
  const { token } = useContext(AuthContext);

  const handleOnChangePasswordSubmit = async (values: ChangePasswordFormValues) => {
    const changePasswordDto: ChangePasswordDto = {
      oldPassword: values.currentPassword,
      newPassword: values.newPassword,
    };

    await axios
      .post("http://localhost:8080/api/v1/user/changePassword", changePasswordDto, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        toast.success("Successful password change.", {
          position: "top-center",
          autoClose: 3000,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
        });
      })
      .catch((error) => {
        toast.error(error.response.data.message, {
          position: "top-center",
          autoClose: 3000,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
        });
      })
      .finally(() => {
        values.currentPassword = "";
        values.newPassword = "";
        values.confirmPassword = "";
        setPasswordDropdownClicked(false);
      });
  };

  useEffect(() => {
    const fetchProfile = async () => {
      await axios
        .get("http://localhost:8080/api/v1/user/getUserProfile", {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((response) => {
          setUserData(response.data);
          const date = new Date(
            response.data.createdAt[0],
            response.data.createdAt[1] - 1,
            response.data.createdAt[2],
            response.data.createdAt[3],
            response.data.createdAt[4],
            response.data.createdAt[5]
          );
          setCreatedAt(date.toLocaleString());
        })
        .catch((error) => {})
        .finally(() => {});
    };

    fetchProfile();
  }, []);

  return (
    <>
      <NavigationBar />
      <div className="container mx-auto p-10">
        <div className="grid grid-cols-1 gap-4 justify-items-center p-4">
          <div className="w-fit">
            <div className="border-2 border-black rounded-full py-6 px-7">
              <FontAwesomeIcon icon={faUser} size={"6x"} />
            </div>
          </div>
          <div>
            <Link to="/profile" className="underline text-blue-500">
              Edit profile picture
              <FontAwesomeIcon icon={faPen} className="ms-2" />
            </Link>
          </div>
          <div className="font-bold text-2xl">
            {userData?.firstName} {userData?.lastName}
          </div>
          <div>{userData?.username}</div>
          <div>Created at: {createdAt}</div>
        </div>

        <div className="border-t-2 border-b-2">
          <div
            className="flex justify-between p-2 hover:bg-gray-100"
            onClick={() => {
              setPasswordDropdownClicked(!passwordDropdownClicked);
            }}
          >
            <div className="font-bold">Change your password</div>
            <div>
              <FontAwesomeIcon icon={passwordDropdownClicked ? faCaretUp : faCaretDown} size={"xl"} />
            </div>
          </div>
          <div
            className={`transition-max-height duration-300 ease-in-out overflow-hidden ${
              passwordDropdownClicked ? "max-h-fit" : "max-h-0"
            }`}
          >
            <Formik
              initialValues={{
                currentPassword: "",
                newPassword: "",
                confirmPassword: "",
              }}
              validationSchema={ChangePasswordSchema}
              onSubmit={(values, formikHelpers) => {
                handleOnChangePasswordSubmit(values);
              }}
            >
              <Form>
                <div className="grid grid-cols-1 justify-items-center gap-4 p-10">
                  <Field
                    name="currentPassword"
                    type="password"
                    placeholder="Current password"
                    className="rounded-md py-4 ps-4 bg-neutral-100 w-2/3"
                  />
                  <ErrorMessage name="currentPassword" component="div" className="text-center text-red-500 pt-4" />
                  <Field
                    name="newPassword"
                    type="password"
                    placeholder="New password"
                    className="rounded-md py-4 ps-4 bg-neutral-100 w-2/3"
                  />
                  <ErrorMessage name="newPassword" component="div" className="text-center text-red-500 pt-4" />
                  <Field
                    name="confirmPassword"
                    type="password"
                    placeholder="Confirm password"
                    className="rounded-md py-4 ps-4 bg-neutral-100 w-2/3"
                  />
                  <ErrorMessage name="confirmPassword" component="div" className="text-center text-red-500 pt-4" />
                  <button
                    type="submit"
                    className="rounded-md bg-amber-600 text-white text-base font-bold py-3 my-8 w-2/3 hover:xl:shadow-lg xl:w-3/12"
                  >
                    Change password
                  </button>
                </div>
              </Form>
            </Formik>
          </div>
        </div>
      </div>
      <ToastContainer />

      <Footer />
    </>
  );
};

export default ProfilePage;
