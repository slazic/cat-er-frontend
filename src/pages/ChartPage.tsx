import CandlestickChart from "../components/CandlestickChart";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars, faMagnifyingGlass, faUser } from "@fortawesome/free-solid-svg-icons";
import { useContext, useEffect, useRef, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import CustomDropdown from "../components/CustomDropdown";
import { Tooltip } from "react-tooltip";
import SearchModal from "../components/SearchModal";
import { fetchSingleCandlestickDataFromSelectedMarket, fetchTickerDataFromAllExchanges } from "../api/fetchExternal";
import { CryptoData, cryptoList } from "../components/homepage/MostPopularList";
import { toast } from "react-toastify";
import { AuthContext } from "../context/AuthProvider";

export interface CryptoExchange {
  name: string;
  logoUrl: string;
}

export const cryptoExchanges: CryptoExchange[] = [
  { name: "Binance", logoUrl: "./market_logo/binance-logo.png" },
  //{ name: "Coinbase", logoUrl: "./market_logo/coinbase-logo.png" },
  { name: "Kraken", logoUrl: "./market_logo/kraken-logo.png" },
];

export interface TickerPriceFromExchange {
  exchangeName: string;
  price: number;
}

export interface BinanceSingleKLinesResponse {
  data: [];
}

const ChartPage = (): JSX.Element => {
  const location = useLocation();
  const [chartInterval, setChartInterval] = useState<string>("W");
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [choosenTicker, setChoosenTicker] = useState<string>(location.state ? location.state.ticker : "BTCEUR");
  const [isWatchlistOpen, setIsWatchlistOpen] = useState<boolean>(true);
  const [choosenMarket, setChoosenMarket] = useState<string>("Binance");
  const [isSearchOpen, setIsSearchOpen] = useState<boolean>(false);
  const [chartData, setChartData] = useState<BinanceSingleKLinesResponse>({ data: [] });
  const [currencyDetails, setCurrencyDetails] = useState<CryptoData>();
  const [loading, setLoading] = useState<boolean>(true);
  const [selectedTickerPrices, setSelectedTickerPrices] = useState<TickerPriceFromExchange[]>([]);
  const searchFieldRef = useRef<HTMLInputElement>(null);
  const navigate = useNavigate();
  const { token } = useContext(AuthContext);

  //used to fetch data
  useEffect(() => {
    const fetchData = async () => {
      //setLoading(true);
      //TODO: refactor in such way so it accepts exchange name as well
      await fetchSingleCandlestickDataFromSelectedMarket(choosenTicker, chartInterval, choosenMarket, token)
        .then((response) => {
          setChartData({ data: response.data });
          setCurrencyDetails(
            cryptoList.filter((cryptoCurrency) => {
              return cryptoCurrency.ticker === choosenTicker?.toUpperCase();
            })[0]
          );
        })
        .catch((error) => {
          console.log(error.response);
          toast.error(error.response.data.message, {
            position: "top-center",
            autoClose: 3000,
            closeOnClick: true,
            pauseOnHover: false,
            draggable: true,
          });
        })
        .finally(() => {
          setLoading(false);
        });
    };

    const fetchSelectedTickerPriceFromDifferentExchanges = async () => {
      //setLoading(true);
      await fetchTickerDataFromAllExchanges(choosenTicker, token)
        .then((response) => {
          setSelectedTickerPrices(response.data);
        })
        .catch((error) => {
          toast.error(error.response.data.message, {
            position: "top-center",
            autoClose: 3000,
            closeOnClick: true,
            pauseOnHover: false,
            draggable: true,
          });
        })
        .finally(() => {
          setLoading(false);
        });
    };

    fetchData();
    fetchSelectedTickerPriceFromDifferentExchanges();
  }, [chartInterval, choosenTicker, choosenMarket]);

  //setup keyboard shortcuts
  useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent) => {
      // Check if a letter key is pressed
      if (event.key && event.key.match(/^[A-Za-z]$/)) {
        setIsSearchOpen(true);
      } else if (event.key === "Escape") {
        setIsSearchOpen(false);
        if (searchFieldRef.current) {
          searchFieldRef.current.value = "";
        }
      }
    };

    // Add the event listener
    window.addEventListener("keydown", handleKeyDown);

    // Remove the event listener when the component unmounts
    return () => {
      window.removeEventListener("keydown", handleKeyDown);
    };
  }, []);

  useEffect(() => {
    if (isSearchOpen && searchFieldRef.current) {
      searchFieldRef.current.focus();
    }
  }, [isSearchOpen]);

  return (
    <>
      <>
        <SearchModal
          isSearchOpen={isSearchOpen}
          setIsSearchOpen={setIsSearchOpen}
          searchFieldRef={searchFieldRef}
          setChoosenMarket={setChoosenMarket}
          setChoosenTicker={setChoosenTicker}
        />
      </>
      <div className="max-h-screen min-h-screen flex flex-col bg-gray-200 text-medium select-none">
        <div className="border-x-2 border-gray-200 bg-white py-1 px-2 select-none">
          <div className="flex justify-between items-center">
            <div className="flex">
              <div
                className="hover:bg-gray-100 px-3 py-2 rounded-md me-2 cursor-pointer"
                id="search-symbol"
                onClick={() => {
                  setIsSearchOpen(true);
                }}
              >
                <FontAwesomeIcon icon={faMagnifyingGlass} /> {choosenTicker}
              </div>
              <Tooltip anchorSelect="#search-symbol" content="Search Symbol" place="bottom-start" />

              <div className="border-r-2 border-gray-100 rounded-full"></div>

              <CustomDropdown chartInterval={chartInterval} setChartInterval={setChartInterval} />

              <div className="border-r-2 border-gray-100 rounded-full"></div>
            </div>

            <div
              className="text-black me-2 cursor-pointer"
              id="check-profile"
              onClick={() => {
                navigate("/profile");
              }}
            >
              <FontAwesomeIcon icon={faUser} size="xl" />
            </div>
            <Tooltip anchorSelect="#check-profile" content={"Profile"} />
          </div>
        </div>

        <div className="border-2 border-gray-200 bg-white">
          <div className="flex flex-row bg-gray-200">
            <div
              className={
                isWatchlistOpen
                  ? "border-2 border-l-0 border-gray-200 rounded-r-lg bg-white sm:w-4/5 w-0"
                  : "border-2 border-l-0 border-gray-200 rounded-r-lg bg-white w-full"
              }
            >
              <CandlestickChart
                chartInterval={chartInterval}
                choosenMarket={choosenMarket}
                chartData={chartData}
                currencyDetails={currencyDetails}
                loading={loading}
              />
            </div>

            <div
              className={
                isWatchlistOpen
                  ? "sm:border-2 sm:border-r-0 sm:w-1/5 w-screen flex border-2 border-r-0 border-l-0"
                  : "border-2 border-r-0 border-gray-200 rounded-l-lg bg-white p-1 w-14"
              }
            >
              {isWatchlistOpen ? (
                <>
                  <div className="bg-white w-full rounded-l-lg">
                    <table className="border w-full">
                      <thead>
                        <tr>
                          <th colSpan={4} className="text-left ps-2 py-2 cursor-default">
                            Watchlist
                          </th>
                        </tr>
                        <tr className="border border-t-0">
                          <td className="text-left ps-2 pb-1 cursor-pointer text-gray-400 ease-in duration-200 hover:text-black">
                            Market
                          </td>
                          <td className="text-right pb-1 cursor-pointer text-gray-400 ease-in duration-200 hover:text-black">
                            Last
                          </td>
                          <td className="text-right pb-1 cursor-pointer text-gray-400 ease-in duration-200 hover:text-black">
                            Diff
                          </td>
                          <td className="text-right pb-1 cursor-pointer text-gray-400 ease-in duration-200 hover:text-black pe-2">
                            Diff %
                          </td>
                        </tr>
                      </thead>
                      <tbody>
                        {cryptoExchanges.map((exchange, index) => {
                          const exchangePrice = Number(
                            selectedTickerPrices.filter((tickerPrice) => tickerPrice.exchangeName === exchange.name)[0]
                              ?.price
                          );

                          const currentSelectedExchangePrice = Number(
                            selectedTickerPrices.filter((tickerPrice) => tickerPrice.exchangeName === choosenMarket)[0]
                              ?.price
                          );

                          const priceDifference = exchangePrice - currentSelectedExchangePrice;

                          const priceDifferencePercentage = (priceDifference / exchangePrice) * 100;

                          return (
                            <tr
                              key={index}
                              className={
                                choosenMarket === exchange.name
                                  ? "border-2 border-e-4 border-blue-600 hover:bg-gray-100 cursor-default"
                                  : "border hover:bg-gray-100 cursor-default"
                              }
                              onClick={() => {
                                setChoosenMarket(exchange.name);
                              }}
                            >
                              <td className="text-left ps-2">
                                <div className="flex items-center">
                                  <div>
                                    <img src={exchange.logoUrl} alt={`${exchange.name}-logo`} className="h-5" />
                                  </div>
                                  <div className="ms-3 text-sm">{exchange.name}</div>
                                </div>
                              </td>
                              <td className="text-right py-1 text-sm">{Number(exchangePrice).toFixed(2)}</td>
                              {priceDifference !== 0 ? (
                                <>
                                  <td
                                    className={
                                      priceDifference < 0
                                        ? "text-right text-green-600 text-sm"
                                        : priceDifference === 0
                                        ? "text-right text-sm"
                                        : "text-right text-red-600 text-sm"
                                    }
                                  >
                                    {Number(Math.abs(priceDifference)).toFixed(2)}
                                  </td>
                                  <td
                                    className={
                                      priceDifference < 0
                                        ? "text-right text-green-600 pe-2 text-sm"
                                        : priceDifference === 0
                                        ? "text-right text-sm"
                                        : "text-right text-red-600 pe-2 text-sm"
                                    }
                                  >
                                    {Number(Math.abs(priceDifferencePercentage)).toFixed(2)}%
                                  </td>
                                </>
                              ) : (
                                <td colSpan={2} className="text-right text-gray-400 pe-2 text-sm">
                                  Currently selected
                                </td>
                              )}
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  </div>
                  <div className="bg-white w-14 border-2 border-gray-100 border-t-0 border-r-0 border-b-0 p-1">
                    <div
                      id="open-watchlist-mobile"
                      className="text-center hover:bg-gray-100 cursor-pointer rounded-lg py-2"
                      onClick={() => {
                        setIsWatchlistOpen(!isWatchlistOpen);
                      }}
                    >
                      <FontAwesomeIcon icon={faBars} size="xl" />
                    </div>
                    <Tooltip anchorSelect="#open-watchlist-mobile" content="Watchlist" place="left" />
                  </div>
                </>
              ) : (
                <>
                  <div
                    id="open-watchlist"
                    className="text-center hover:bg-gray-100 cursor-pointer py-2 rounded-lg"
                    onClick={() => {
                      setIsWatchlistOpen(!isWatchlistOpen);
                    }}
                  >
                    <FontAwesomeIcon icon={faBars} size="xl" />
                  </div>
                  <Tooltip anchorSelect="#open-watchlist" content="Watchlist" place="left" />
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ChartPage;
